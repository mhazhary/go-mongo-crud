package controllers

import (
	"context"
	"log"
	"strconv"

	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"go-mongo-crud/database"

	helper "go-mongo-crud/helpers"
	"go-mongo-crud/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var catCollection *mongo.Collection = database.OpenCollection(database.Client, "cat")

//AddCat is the API used to add cat to a single user
func AddCat() gin.HandlerFunc {
	return func(c *gin.Context) {
		userId := c.Param("user_id")

		if err := helper.MatchUserTypeToUid(c, userId); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		var ctx, cancel = context.WithTimeout(context.Background(), 100*time.Second)
		var cat models.Cat

		defer cancel()
		if err := c.BindJSON(&cat); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		validationErr := validate.Struct(cat)
		if validationErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": validationErr.Error()})
			return
		}

		count, err := catCollection.CountDocuments(ctx, bson.M{"user_id": cat.User_id, "cat_name": cat.Cat_name})
		defer cancel()
		if err != nil {
			log.Panic(err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": "error occurred while checking for the email"})
			return
		}

		if count > 0 {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "this email already exists"})
			return
		}

		cat.ID = primitive.NewObjectID()
		cat.Created_at, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))
		cat.Updated_at, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))
		cat.User_id = userId

		resultInsertionNumber, insertErr := catCollection.InsertOne(ctx, cat)
		if insertErr != nil {
			msg := "Cat item was not created"
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
			return
		}
		defer cancel()
		c.JSON(http.StatusOK, resultInsertionNumber)
	}
}

//GetCats is the API used to get all cats from all users
func GetCats() gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := helper.CheckUserType(c, "ADMIN"); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		var ctx, cancel = context.WithTimeout(context.Background(), 100*time.Second)

		recordPerPage, err := strconv.Atoi(c.Query("recordPerPage"))
		if err != nil || recordPerPage < 1 {
			recordPerPage = 10
		}

		page, err1 := strconv.Atoi(c.Query("page"))
		if err1 != nil || page < 1 {
			page = 1
		}

		var startIndex = (page - 1) * recordPerPage
		startIndex, _ = strconv.Atoi(c.Query("startIndex"))

		matchStage := bson.D{
			{
				Key:   "$match",
				Value: bson.D{{}},
			},
		}
		groupStage := bson.D{
			{
				Key: "$group",
				Value: bson.D{
					{
						Key: "_id",
						Value: bson.D{
							{
								Key:   "_id",
								Value: "null",
							},
						},
					}, {
						Key: "total_count",
						Value: bson.D{
							{
								Key:   "$sum",
								Value: 1,
							},
						},
					}, {
						Key: "data",
						Value: bson.D{
							{
								Key:   "$push",
								Value: "$$ROOT",
							},
						},
					},
				},
			},
		}
		projectStage := bson.D{
			{
				Key: "$project",
				Value: bson.D{
					{
						Key:   "_id",
						Value: 0,
					}, {
						Key:   "total_count",
						Value: 1,
					}, {
						Key: "cat_items",
						Value: bson.D{
							{
								Key:   "$slice",
								Value: []interface{}{"$data", startIndex, recordPerPage},
							},
						},
					},
				},
			},
		}

		result, err := catCollection.Aggregate(ctx, mongo.Pipeline{
			matchStage, groupStage, projectStage})
		defer cancel()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "error occured while listing cat items"})
		}
		var allcats []bson.M
		if err = result.All(ctx, &allcats); err != nil {
			log.Fatal(err)
		}
		c.JSON(http.StatusOK, allcats[0])
	}
}

//GetCat is the API used to get all cats from a single user
func GetCat() gin.HandlerFunc {
	return func(c *gin.Context) {
		userId := c.Param("user_id")

		if err := helper.MatchUserTypeToUid(c, userId); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		var ctx, cancel = context.WithTimeout(context.Background(), 100*time.Second)

		recordPerPage, err := strconv.Atoi(c.Query("recordPerPage"))
		if err != nil || recordPerPage < 1 {
			recordPerPage = 10
		}

		page, err1 := strconv.Atoi(c.Query("page"))
		if err1 != nil || page < 1 {
			page = 1
		}

		var startIndex = (page - 1) * recordPerPage
		startIndex, _ = strconv.Atoi(c.Query("startIndex"))

		matchStage := bson.D{
			{
				Key: "$match",
				Value: bson.D{
					{
						Key:   "user_id",
						Value: userId,
					},
				},
			},
		}
		groupStage := bson.D{
			{
				Key: "$group",
				Value: bson.D{
					{
						Key: "_id",
						Value: bson.D{
							{
								Key:   "_id",
								Value: "null",
							},
						},
					}, {
						Key: "total_count",
						Value: bson.D{
							{
								Key:   "$sum",
								Value: 1,
							},
						},
					}, {
						Key: "data",
						Value: bson.D{
							{
								Key:   "$push",
								Value: "$$ROOT",
							},
						},
					},
				},
			},
		}
		projectStage := bson.D{
			{
				Key: "$project",
				Value: bson.D{
					{
						Key:   "_id",
						Value: 0,
					}, {
						Key:   "total_count",
						Value: 1,
					}, {
						Key: "cat_items",
						Value: bson.D{
							{
								Key:   "$slice",
								Value: []interface{}{"$data", startIndex, recordPerPage},
							},
						},
					},
				},
			},
		}

		result, err := catCollection.Aggregate(ctx, mongo.Pipeline{
			matchStage, groupStage, projectStage})
		defer cancel()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "error occured while listing cat items"})
		}
		var allcats []bson.M
		if err = result.All(ctx, &allcats); err != nil {
			log.Fatal(err)
		}
		c.JSON(http.StatusOK, allcats[0])
	}
}
