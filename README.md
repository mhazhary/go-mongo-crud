# Go Mongo CRUD

A simple CRUD REST API with MongoDB and JWT-based authentication.
> This is my first time to use MongoDB, feel free to give a comment or point an issue. This repo is made possible by [Joojo7/user-athentication-golang](https://github.com/Joojo7/user-athentication-golang) repository as reference.

## My Personal TODO

- [ ] Some error is not handled properly
- [ ] Better query for MongoDB?
- [ ] Try to make it a fundamentally true CRUD (Only C and R method are implemented right now)

## Running Locally

First thing first, clone this repository and build the Go binary from the source.

```sh
git clone https://gitlab.com/mhazhary/go-mongo-crud.git
cd go-mongo-crud
go build -o bin/ -v 
```

Before running the binary, setup your `.env` file with this format and put it in the folder's root. You can use this [handy website](https://www.grc.com/passwords.htm) to generate the `SECRET_KEY` or put your own random key.

```sh
PORT=               # Without defined PORT, default port is set at 8000
MONGODB_URL=
SECRET_KEY=
GIN_MODE=release    # You can omit this line for more verbose output
```

Now you can run the binary.

```sh
./bin/go-mongo-crud
```

The application should be available and running on [localhost:8000](http://localhost:8000/) (or on your own `PORT`).

## Container Image

You can pull the container image from [registry.gitlab.com/mhazhary/go-mongo-crud](registry.gitlab.com/mhazhary/go-mongo-crud) 