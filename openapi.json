{
  "openapi": "3.0.0",
  "info": {
    "title": "go-mongo-crud API Docs",
    "version": "1.0.0",
    "description": "Documentations for a simple CRUD backend.",
    "contact": {
      "email": "hanif@azhary.dev"
    },
    "license": {
      "name": "MIT License",
      "url": "https://gitlab.com/mhazhary/go-mongo-crud/-/blob/main/LICENSE"
    }
  },
  "tags": [
    {
      "name": "user",
      "description": "Everything about user auth"
    },
    {
      "name": "cats",
      "description": "Everything about cat"
    }
  ],
  "paths": {
    "/users/signup": {
      "post": {
        "operationId": "SignUp",
        "summary": "Sign up a new user",
        "tags": [
          "user"
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserSignUp"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "as ADMIN": {
                    "value": {
                      "InsertedID": "62dfd0cf0c947733f135a863"
                    }
                  },
                  "as USER": {
                    "value": {
                      "InsertedID": "62dfd0cf0c947733f135a863"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/users/login": {
      "post": {
        "operationId": "Login",
        "summary": "Log in a user",
        "tags": [
          "user"
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserLogin"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "ID": "62dfd0cf0c947733f135a863",
                      "full_name": "Admin",
                      "password": "$hashed_password$",
                      "email": "admin@google.com",
                      "token": "$jwt_token$",
                      "user_type": "ADMIN",
                      "refresh_token": "$jwt_refresh_token$",
                      "created_at": "2022-07-26T11:32:31Z",
                      "updated_at": "2022-07-26T11:35:12Z",
                      "user_id": "62dfd0cf0c947733f135a863"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/users": {
      "get": {
        "operationId": "GetUsers",
        "summary": "Get all users",
        "tags": [
          "user"
        ],
        "parameters": [
          {
            "in": "header",
            "name": "token",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "total_counts": 4,
                      "user_items": [
                        [
                          "User Object 1",
                          "User Object 2"
                        ]
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/users/{user_id}": {
      "get": {
        "operationId": "GetUser",
        "summary": "Get an user from specific User ID",
        "tags": [
          "user"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "user_id",
            "schema": {
              "type": "string"
            },
            "required": true
          },
          {
            "in": "header",
            "name": "token",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "total_counts": 4,
                      "user_items": [
                        [
                          "User Object 1",
                          "User Object 2"
                        ]
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/cats": {
      "get": {
        "operationId": "GetCats",
        "summary": "Get all cats",
        "tags": [
          "cats"
        ],
        "parameters": [
          {
            "in": "header",
            "name": "token",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "total_counts": 4,
                      "user_items": [
                        "Cat Object 1",
                        "Cat Object 2"
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/cats/{user_id}": {
      "get": {
        "operationId": "GetCat",
        "summary": "Get all cat from specific User ID",
        "tags": [
          "cats"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "user_id",
            "schema": {
              "type": "string"
            },
            "required": true
          },
          {
            "in": "header",
            "name": "token",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "total_counts": 4,
                      "user_items": [
                        "Cat Object 1",
                        "Cat Object 2"
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      },
      "post": {
        "operationId": "AddCat",
        "summary": "Add cat to an user",
        "tags": [
          "cats"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "user_id",
            "schema": {
              "type": "string"
            },
            "required": true
          },
          {
            "in": "header",
            "name": "token",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CatAddCat"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "total_counts": 4,
                      "user_items": [
                        [
                          "Cat Object 1",
                          "Cat Object 2"
                        ]
                      ]
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/try": {
      "get": {
        "operationId": "Echo",
        "summary": "Reply when user authentication successful",
        "tags": [
          "default"
        ],
        "parameters": [
          {
            "in": "header",
            "name": "token",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "200 response",
            "content": {
              "application/json": {
                "examples": {
                  "Login": {
                    "value": {
                      "success": "Access granted"
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "UserSignUp": {
        "properties": {
          "full_name": {
            "type": "string"
          },
          "email": {
            "type": "string",
            "format": "email"
          },
          "password": {
            "type": "string",
            "format": "password"
          },
          "user_type": {
            "type": "string",
            "enum": [
              "ADMIN",
              "USER"
            ]
          }
        }
      },
      "UserLogin": {
        "properties": {
          "email": {
            "type": "string",
            "format": "email"
          },
          "password": {
            "type": "string",
            "format": "password"
          }
        }
      },
      "CatAddCat": {
        "properties": {
          "cat_name": {
            "type": "string"
          }
        }
      }
    }
  }
}