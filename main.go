package main

import (
	"fmt"
	"os"

	routes "go-mongo-crud/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		fmt.Println("Default port 8000 will be used because PORT environment variables is not set")
		port = "8000"
	}

	router := gin.New()
	router.Use(gin.Logger())

	routes.AuthRoutes(router)
	routes.UserRoutes(router)
	routes.CatRoutes(router)

	// Endpoint for testing the authentication
	router.GET("/try", func(c *gin.Context) {
		c.JSON(200, gin.H{"success": "Access granted"})
	})
	router.Run(":" + port)
}
