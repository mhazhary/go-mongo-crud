package routes

import (
	controller "go-mongo-crud/controllers"
	"go-mongo-crud/middleware"

	"github.com/gin-gonic/gin"
)

//CatRoutes function
func CatRoutes(incomingRoutes *gin.Engine) {
	incomingRoutes.Use(middleware.Authentication())
	incomingRoutes.GET("/cats", controller.GetCats())
	incomingRoutes.GET("/cats/:user_id", controller.GetCat())
	incomingRoutes.POST("/cats/:user_id", controller.AddCat())
}
